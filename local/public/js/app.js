angular.module('app', [])

.controller('myCtrl', function($scope, $http) {
    $http.get("/dilucion-md/getProducts")
    .then(function(response) {
        //First function handles success
        $scope.content = response.data;
        //console.log($scope.content[0])
        $scope.datos = $scope.content[0];

    }, function(response) {
        //Second function handles error
        $scope.content = "Algo ocurrió. Contacte al administrador.";
    });

    $scope.total = function(){
        return (($scope.volumen * 1000 ) / ($scope.percentage + 1));
    };
})
