<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dilution extends Model
{
    protected $table = 'dilutions';

    protected $fillable = ['name','product_id',];
}
