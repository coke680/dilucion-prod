<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Dilution;
use App\Product;
use Session;
use DB;

class DilutionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dilutions = DB::table('dilutions')
            ->join('products', 'dilutions.product_id', '=', 'products.id')
            ->select('dilutions.id as dilution_id','dilutions.name as dilution_name',
                     'dilutions.product_id', 'products.name as product_name')
            ->orderBy('product_name', 'desc')
            ->get();
        return view('dilutions.index', compact('dilutions'));
        //return $dilutions;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dilutions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:1000|min:4',
            'product_id' => 'required',
       
        ]);

        Dilution::create([            
            'name' => $request['name'],
            'product_id' => $request['product_id'],
        ]);

        Session::flash('success', 'Recomendación creada exitosamente');
        return redirect()->to('dilutions/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dilution = Dilution::find($id);
        return view('dilutions.edit', compact('dilution'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:1000|min:4',
            'product_id' => 'required',
        ]);

        $dilution = Dilution::find($id);

        $dilution->update(request()->all());
        Session::flash('success', 'Recomendación editada exitosamente');
        return redirect()->to('dilutions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $dilution = Dilution::find($id);

            $dilution->delete();
            Session::flash('warning', 'La recomendación ha sido eliminada exitosamente');
            return back();
        }
        catch(\Illuminate\Database\QueryException $e) {
            Session::flash('error', 'Error. Recomendación asociada a algún registro');
            return back();
        }
    }
}
