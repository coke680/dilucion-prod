<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Product;
use DB;
use Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProducts() {

        $products = DB::table('products')
            ->select('products.id as product_id','products.name as product_name',
                     'products.description as product_description','products.provider as product_provider')
            ->get();

        return $products;        
    }

    public function getProductsDetail($id) {
        $dilutions_by_product = DB::table('dilutions')
            ->select('dilutions.id','dilutions.name','dilutions.product_id')
            ->where('dilutions.product_id','=', $id)
            ->get();

        return $dilutions_by_product;

    }

    public function index() {
        $products = DB::table('products')
            ->select('products.id as product_id','products.name as product_name',
                     'products.description as product_description','products.provider as product_provider')
            ->get();

        return view('products.index', compact('products'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $this->validate($request, [
            'description' => 'required|max:1000|min:4',
            'name' => 'required|max:200|min:4',
            'provider' => 'required|max:200|min:4',
       
        ]);

        Product::create([
            'description' => $request['description'],
            'name' => $request['name'],
            'provider' => $request['provider'],
        ]);

        Session::flash('success', 'Producto creado exitosamente');
        return redirect()->to('products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'description' => 'required|max:1000|min:4',
            'name' => 'required|max:200|min:4',
            'provider' => 'required|max:200|min:4',
        ]);

        $product = Product::find($id);

        $product->update(request()->all());
        Session::flash('success', 'Producto editado exitosamente');
        return redirect()->to('products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $product = Product::find($id);

            $product->delete();
            Session::flash('warning', 'Producto eliminado exitosamente');
            return back();
        }
        catch(\Illuminate\Database\QueryException $e) {
            Session::flash('error', 'Error. producto asociado a algún registro');
            return back();
        }
    }
}
