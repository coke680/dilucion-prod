@extends ('layouts.app')

@section('content')

	<div class="row">
	<div class="col-lg-6">

		@if (Session::has('error'))
	        <div class="alert alert-danger alert-dismissible" role="alert">
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <strong>{{ Session::get('error') }}</strong>
	        </div>
	    @endif

	    @if (Session::has('warning'))
	        <div class="alert alert-warning alert-dismissible" role="alert">
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <strong>{{ Session::get('warning') }}</strong>
	        </div>
	    @endif

	    @if (Session::has('success'))
	        <div class="alert alert-success alert-dismissible" role="alert">
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <strong>{{ Session::get('success') }}</strong>
	        </div>
	    @endif
    
		<form action="{{ url('/dilutions') }}" method="POST" role="form">
			<h3>Ingresar recomendaciones para la dilución</h3><hr>

			{{ csrf_field() }}

			<div class="form-group {{ $errors->has('product_id') ? ' has-error' : '' }}">
				<select name="product_id" id="input" class="form-control chosen" required>
					<option value="">- Seleccione un producto -</option>
					@foreach(\App\Product::select('id', 'name')->get() as $product)
						<option value="{{ $product->id }}">{{ $product->name }}</option>
					@endforeach
				</select>
				@if ($errors->has('product_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('product_id') }}</strong>
                    </span>
                @endif
			</div>

			<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
				<textarea type="text" class="form-control" required id="name" name="name" value="{{ old('name') }}" placeholder="Describa la recomendación para el producto.">@if ($errors->has('name'))<span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>@endif</textarea>
			</div>


			<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar recomendación</button>
			<a href="/dilucion-md/dilutions" class="btn btn-default"><i class="fa fa-close"></i> Cancelar</a>
		</form>
	</div>

</div>
@endsection

@push('script')

<script>
	$(document).ready(function(){
		$(".chosen").select2({
			"language": {
		       "noResults": function(){
		           return "Resultado no encontrado.";
		       }
		   },
		    escapeMarkup: function (markup) {
		        return markup;
		    }
		});
	});
</script>

@endpush