@extends('layouts.app')

@section('content')
     <h2 class="text-center">Sistema para dilución de productos de aseo</h2><hr>
      <div ng-controller="myCtrl">
        <div class="container">
          <div class="row">
            <form name="myForm" class="col-lg-4 col-xs-12">
              <label for="mySelect">Seleccione producto:</label>
              <select name="mySelect" id="mySelect"
                ng-options="option.product_name for option in content track by option.product_id"
                ng-model="datos" class="form-control"></select>
            </form>
          </div><hr>
          <div class="row">
            <div class="col-lg-12 table-responsive">
              <table class="table table-hover table-bordered">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Proveedor</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td ng-cloak>@{{datos.product_name}}</td>
                    <td ng-cloak>@{{datos.product_description}}</td>
                    <td ng-cloak>@{{datos.product_provider}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div><hr>
          <div class="row">
              <div class="col-lg-12 col-xs-12">
                 <a class="btn btn-md btn-primary" ng-cloak id="@{{datos.product_id}}" data-toggle="modal" href='#modal-id'><i class="fa fa-btn fa-eye"></i> Recomendaciones de @{{datos.product_name}}</a>
                    <div class="modal fade" id="modal-id" ng-cloak>
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Recomendaciones para @{{datos.product_name}}</h4>
                          </div>
                          <div class="modal-body detail">
                                                     
                                
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-md btn-primary" data-dismiss="modal">Cerrar</button>
                          </div>
                        </div>
                      </div>
                    </div>
              </div>
            </div><hr>
          <div class="row">
          <div class="col-lg-12 col-xs-12">
            <h4><i class="fa fa-calculator"></i> Calcular dilución</h4><hr>
                <form>
                  <div class="row">          
                    <div class="form-group col-lg-3">
                      <input class="form-control" type="number" ng-model="percentage" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" placeholder="Dilución 1 a:"><br>

                      <p class="alert-info alert" ng-cloak>Dilución 1:@{{percentage}}</p>

                    </div>
                    <div class="form-group col-lg-3">
                      <input class="form-control" type="number" ng-model="volumen" min="0" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" placeholder="Volúmen del envase"><br>

                      <p class="alert-info alert" ng-cloak>Volúmen del envase: @{{volumen}} 
                          <span ng-if="volumen == 1">
                          litro
                          </span>
                          <span ng-if="volumen > 1">
                          litros
                          </span>
                      </p>

                    </div> 
                    <div class="col-lg-6">
                      <h4>Cantidad de producto:</h4>
                      <h4 class="alert alert-success" ng-cloak>
                          <span ng-if="volumen == null || percentage == null">
                           0 ml (0 Lt)
                          </span>
                          <span ng-if ="volumen != null && percentage != null">
                            <span>                           
                                @{{ total() | number : 1 }}</span> ml
                            <span>                           
                                ( @{{ total() / 1000 | number : 1}} Lt)</span>
                          </span>                          
                      </h4>
                    </div> 
                  </div>    
                </form>
    
          </div>
        </div>
      </div>

@endsection

@push('getdata')
  <script>

    $('#modal-id').on('show.bs.modal', function(e) {
        var $id = e.relatedTarget.id;
        //console.log($id);
        // $('#texto').text('El valor es ' + $id);
         $.ajax({
            type: 'GET',
            url: 'products/getProductsDetail/' + $id,
            dataType: 'JSON',
            })

        .done(function(data) {
            var details = data;
            var $detail = "";
            $(".detail").empty();
            $.each(details, function(key, value) {            
                $detail += '<p value="'+value.id+'><i class="fa fa-edit"></i> - '+value.name+'</p>';
            });

            $('.detail').append($detail);

        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    });

</script>
@endpush