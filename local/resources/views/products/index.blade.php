@extends ('layouts.app')

@section('content')

	@if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('error') }}</strong>
        </div>
    @endif

    @if (Session::has('warning'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('warning') }}</strong>
        </div>
    @endif

    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('success') }}</strong>
        </div>
    @endif
    
	<h3>Listado de productos <a class="pull-right btn btn-sm btn-success" href="products/create"><i class="fa fa-plus-circle fa-btn"></i> Crear producto</a></h3><hr>
    
	<div class="table-responsive">
		<table class="table table-striped table-bordered" id="ProductTable">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Descripción</th>
					<th>Proveedor</th>
					<th>Dilución</th>
                    <th>Opciones</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($products as $product) 
				<tr>					
					<td>{{ $product->product_name }}</td>
					<td>{{ $product->product_description }} </td>
					<td>{{ $product->product_provider }}</td>
                    <td><a class="btn btn-xs btn-primary" id="{{ $product->product_id }}" data-toggle="modal" href='#modal-id'>Ver recomendaciones</a>

                    <div class="modal fade" id="modal-id">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Recomendaciones:</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="detail">
                                        
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-md btn-default" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div></td>
					<td><a href="products/edit/{{ $product->product_id }}" class="btn btn-xs btn-warning margin-button"><i class="fa fa-edit fa-btn"></i> Editar</a><a href="products/del/{{ $product->product_id }}" onclick="return confirm('¿Está seguro de eliminar este producto?')" class="btn btn-xs btn-danger"><i class="fa fa-close fa-btn"></i> Eliminar</a></td> 				
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>

<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>

@endsection

@push('datatable')
  <script>

    $(document).ready(function(){
            $('#ProductTable').DataTable({

            responsive: true,
            processing: true,
            bLengthChange: false,

            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "search": "Filtrar:",
                "zeroRecords": "No encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No existen registros",
                "infoFiltered": "(Filtrando _MAX_ registros)",
                "paginate": {
                    "previous": "Página anterior",
                    "next": "Página siguiente"
                  },
            },
            // dom: 'Bfrtip',
            // buttons: [
            //     { extend: 'pdf', className: 'btn btn-default margin-button', exportOptions: {
            //         columns: [ 0, 1, 2 ]
            //     } },
            //     { extend: 'excel', className: 'btn btn-default', exportOptions: {
            //         columns: [ 0, 1, 2 ]
            //     } },

            // ],
            columnDefs: [
                { width: 50, targets: 0 },
                { width: 100, targets: 1 },
                { width: 100, targets: 2 },
                { width: 100, targets: 3 },
                { width: 100, targets: 4 }
            ],
            fixedColumns: true,

        });

        $('#modal-id').on('show.bs.modal', function(e) {
            var $id = e.relatedTarget.id;

             $.ajax({
                type: 'GET',
                url: 'products/getProductsDetail/' + $id,
                dataType: 'JSON',
                })

            .done(function(data) {
                var details = data;
                var $detail = "";
                $(".detail").empty();
                $.each(details, function(key, value) {            
                    $detail += '<p value="'+value.id+'><i class="fa fa-edit"></i> - '+value.name+'</p>';
                });

                $('.detail').append($detail);

            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
        });

    });
</script>
@endpush