@extends ('layouts.app')

@section('content')

	<div class="row">
	<div class="col-lg-6">
		<form action="{{ url('/products') }}" method="POST" role="form">
			<h3>Ingresar Productos</h3><hr>

			{{ csrf_field() }}

			<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
				<input type="text" class="form-control" required id="name" name="name" value="{{ old('name') }}" placeholder="Nombre del producto">
				@if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
			</div>

			<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
				<input type="text" class="form-control" required id="description" value="{{ old('description') }}" name="description" placeholder="Descripción">
                @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
			</div>

			<div class="form-group {{ $errors->has('provider') ? ' has-error' : '' }}">
				<input type="text" class="form-control" required id="provider" name="provider" value="{{ old('provider') }}" placeholder="Proveedor">
				@if ($errors->has('provider'))
                    <span class="help-block">
                        <strong>{{ $errors->first('provider') }}</strong>
                    </span>
                @endif
			</div>	

			<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar producto</button>
			<a href="/dilucion-md/products" class="btn btn-default"><i class="fa fa-close"></i> Cancelar</a>
		</form>
	</div>

</div>
@endsection